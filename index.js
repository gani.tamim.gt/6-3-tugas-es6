// soal no 1
console.log("soal no 1");

const kelPersegi = (sisi) => 4 * sisi;
const LuasPersegi = (sisi) => sisi * sisi;

console.log(`Keliling persegi :${kelPersegi(2)}`);
console.log(`Luas persegi :${LuasPersegi(2)}`);


// soal no 2
console.log("\nsoal no 2");

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => console.log(firstName + " " + lastName)
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()


// soal no 3
console.log("\nsoal no 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// soal no 4
console.log("\nsoal no 4");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combinedArray = [...west, ...east];
//Driver Code
console.log(combinedArray);

// soal no 5
console.log("\nsoal no 5");

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, ' + 'consectetur adipiscing elit, ${planet}`;
console.log(before);